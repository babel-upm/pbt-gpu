#include <stdio.h>
#include <curand.h>
#include <curand_kernel.h>
#include <time.h>

#define BLOCK_SIZE 1024
#define N_BLOCKS 256
#define N (BLOCK_SIZE * N_BLOCKS)

#define R 382
#define SEED 3123
#define MAX_VALUE 100
#define ADT_SIZE 24

#define NIL 0
#define CONS 1

#define IS_NIL(x) x % 5 == 0
#define IS_CONS(x) x % 5 != 0

void print_list(int *a, int size) {
  printf("[ ");
  for (int i = 0; i < size; i++) {
      printf("%02d ", a[i]);
  }
  printf("]\n");
}

int count_nz(int *a, int size) {
  int counter = 0;
  int i=0;
  for(;i < size; i++){
    if (a[i] >= 0) {
      counter++;
    }
  }
  return counter;
}

int max_sorted(int *a, int size) {
  int max_size = -1;
  int i;
  for(i = 0; i < size; i++){
    max_size = max(max_size, a[i]);
  }
  return max_size;
}

void print_adt_list(int *a, int size) {
  printf("[\n");
  for (int i = 0; i < size; i++) {
    if (ADT_SIZE <= 64 || i % ADT_SIZE < 10) {
      printf("%02d ", a[i]);
    }
    if ((i + 1) % ADT_SIZE == 0) {
      if (ADT_SIZE > 64) {
        printf("...");
      }
      printf("\n");
    }
  }
  printf("]\n");
}

/* For the highest quality parallel pseudorandom number generation, each
   experiment should be assigned a unique seed. Within an experiment, each
   thread of computation should be assigned a unique sequence number. If an
   experiment spans multiple kernel launches, it is recommended that threads
   between kernel launches be given the same seed, and sequence numbers be
   assigned in a monotonically increasing way. If the same configuration of
   threads is launched, random state can be preserved in global memory between
   launches to avoid state setup time.

   https://docs.nvidia.com/cuda/curand/device-api-overview.html#pseudorandom-sequences

*/
__global__ void init_random_states(curandState* states, int seed) {
  // int tid = threadIdx.x;
  int tid = blockIdx.x * blockDim.x + threadIdx.x;
  curand_init(seed, tid, 0, &states[tid]);
}

__global__ void gen_random_int(curandState *states, int *out) {
  // int tid = threadIdx.x;
  int tid = blockIdx.x * blockDim.x + threadIdx.x;
  out[tid] = (int) floor(curand_uniform(&states[tid]) * MAX_VALUE);
}

__global__ void gen_random_adt(curandState *states, int *out) {
  // int tid = threadIdx.x;
  int tid = blockIdx.x * blockDim.x + threadIdx.x;
  int value;
  int i;

  /* Use a for loop because we have a random generator for each ADT. We could
     use any, but this might be easy for reproducibility. We have the hope to
     simply send back the initial states that works to generate and populate
     compute again the value on CPU. (If this is cheaper than whole memory
     transfer).
  */
  for (i=0; i < ADT_SIZE; i++) {
    /*
       I am not sure about how to write in memmory. I think that consecute
       threads writting to consecutive memory addresses is better, altough the
       calculation seems counter-intuitive at first. Also, not sure on later
       performance on property check.

       But, here are both cases:

       Memory array:
       [0, 1, 2, 3, ..., 9999]

       Cunks for each ADT (of size 100):
       [  0,   1, ...,  99,
        100, 101, ..., 199,
        ...,         , 9999 ]

       | tid | ADT_SIZE * tid + i |
       |-----|--------------------|
       | 0   |   0,   1,   2, ... |
       | 1   | 100, 101, 102, ... |
       | 2   | 200, 201, 202, ... |

       | tid | ADT_SIZE * i + tid |
       |-----|--------------------|
       | 0   | 0, 100, 200, ...   |
       | 1   | 1, 101, 201, ...   |
       | 2   | 2, 102, 202, ...   |

       I will go with the first one. But some profiling is pending... Probably
       the second option is better.
    */
    value = (int) floor(curand_uniform(&states[tid]) * MAX_VALUE);
    /*
      I had an error here: I was doing N * tid + 1 instead of using ADT_SIZE.  I
      use as debug: tid, and tid % 2 to see alternating patterns. Also the same
      value expected.
    */
    out[ADT_SIZE * tid + i] = value;
  }
}

__global__ void prop_sorted(int* values, int* result) {
  // int tid = threadIdx.x;
  int tid = blockIdx.x * blockDim.x + threadIdx.x;
  int pos = 0;

  int sorted_size = -1;

  int curr_cons = values[tid * ADT_SIZE + pos + 0];
  if (IS_NIL(curr_cons)) {
    result[tid] = 0;
    return;
  }
  // curr_cons is CONS
  sorted_size = 1;
  while (pos < ADT_SIZE - 2 && sorted_size >= 0) {
    int curr_val  = values[tid * ADT_SIZE + pos + 1];
    int next_cons = values[tid * ADT_SIZE + pos + 2];
    int next_val  = values[tid * ADT_SIZE + pos + 3];

    if (IS_NIL(next_cons)) {
      pos = ADT_SIZE; // Skip to the end because we have finished
    } else {
      // next_cons is CONS
      if (curr_val <= next_val) {
        sorted_size++;
      } else {
        sorted_size = -1; // It is not sorted
      }
      pos += 2;
    }
  }

  __syncthreads();
  result[tid] = sorted_size;
}
/******** END OF KERNELS ********/


/********   BEGIN MAIN   ********/

int main(void) {
  // Allocation (host random states + device random states)
  int * values;
  int * d_values;
  int values_size = N * ADT_SIZE * sizeof(int);

  curandState* d_states;
  int states_size = N * sizeof(curandState);

  int * sorted_result;
  int * d_sorted_result;
  int sorted_result_size = N * sizeof(int);

  // Host memory
  values = (int*) malloc(values_size);
  sorted_result = (int*) malloc(sorted_result_size);

  // Device memory
  cudaMalloc(&d_values, values_size);
  cudaMalloc(&d_states, states_size);
  cudaMalloc(&d_sorted_result, sorted_result_size);

  /* TIMERS START */
  clock_t t;
  t = clock();

  /* RUN */
  init_random_states<<<N_BLOCKS,BLOCK_SIZE>>>(d_states, SEED);

  int r;
  int sorted_all = 0;
  int sorted_max_len = -1;
  for(r=0;r<R;r++) {
    gen_random_adt<<<N_BLOCKS,BLOCK_SIZE>>>(d_states, d_values);
    prop_sorted<<<N_BLOCKS,BLOCK_SIZE>>>(d_values,d_sorted_result);
    // prop_sorted<<<N_BLOCKS,BLOCK_SIZE>>>(d_values,d_sorted_result);
    // cudaMemcpy(values, d_values, values_size, cudaMemcpyDeviceToHost);
    cudaMemcpy(sorted_result, d_sorted_result, sorted_result_size, cudaMemcpyDeviceToHost);
    // printf("Result: ");
    // print_adt_list(values, N * ADT_SIZE);
    // print_list(sorted_result, N);
    sorted_all += count_nz(sorted_result, N);
    sorted_max_len = max(sorted_max_len, max_sorted(sorted_result, N));
  }

  /* TIMERS END */
  t = clock() - t;
  double time_taken = ((double)t)/CLOCKS_PER_SEC; // calculate the elapsed time
  double list_per_second = ((double) sorted_all) / time_taken;

  // OUTPUT
  printf("Result: %9d generated lists (%.02f seconds)\n", N * R, time_taken);
  printf(" found: %9d sorted lists (%.04e lists/second)\n", sorted_all, list_per_second);
  printf("   max: %3d max length\n", sorted_max_len);
  printf("CUDA  & %9d & %.02f & %.04e \n",sorted_all, time_taken, list_per_second);

  // Clean
  free(values);free(sorted_result);
  cudaFree(d_values); cudaFree(d_states); cudaFree(d_sorted_result);

  return 0;
}