#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h> // Used for seed of srand

#define BLOCK_SIZE 1024
#define N_BLOCKS 256
#define N (BLOCK_SIZE * N_BLOCKS)

#define R (382 * N)
#define SEED 3123
#define MAX_VALUE 1000
#define ADT_SIZE 56

#define MAX_LENGTH (ADT_SIZE / 2)

#define NIL 0
#define CONS 1

#define IS_NIL(x) x % 5 == 0
#define IS_CONS(x) x % 5 != 0

#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))
#define MAX(X, Y) (((X) > (Y)) ? (X) : (Y))

void print_list(int *a, int size) {
  printf("[ ");
  for (int i = 0; i < size; i++) {
      printf("%02d ", a[i]);
  }
  printf("]\n");
}

void print_list_double(double *a, int size) {
  printf("[ ");
  for (int i = 0; i < size; i++) {
      printf("%.04f ", a[i]);
  }
  printf("]\n");
}

void print_adt_list(int *a, int size) {
  printf("[\n");
  for (int i = 0; i < size; i++) {
    if (ADT_SIZE <= 64 || i % ADT_SIZE < 10) {
      printf("%02d ", a[i]);
    }
    if ((i + 1) % ADT_SIZE == 0) {
      if (ADT_SIZE > 64) {
        printf("...");
      }
      printf("\n");
    }
  }
  printf("]\n");
}

void fill_random_list(int *a, int size) {
  int i;
  for (i = 0; i < size; ++i) {
    a[i] = rand() % MAX_VALUE;
  }
}

int host_prop_sorted(int* values, int size) {
  int sorted_size = -1;
  int pos = 0;

  int curr_cons = values[pos]; // 0 -> Nil, 1 -> Cons
  if (IS_NIL(curr_cons)) {
    return 0;
  }
  // curr_cons is CONS
  sorted_size = 1;
  while (pos < size - 2 && sorted_size >= 0) {
    int next_cons = values[pos + 2];

    if (IS_NIL(next_cons)) {
      pos = ADT_SIZE; // Skip to the end because we have finished
    } else {
      // next_cons is CONS
      int curr_val  = values[pos + 1];
      int next_val  = values[pos + 3];
      if (curr_val <= next_val) {
        sorted_size++;
      } else {
        sorted_size = -1; // It is not sorted
      }
      pos += 2;
    }
  }
  return sorted_size;
}

int host_prop_length(int* values, int max_size) {
  int size = -1;
  int pos = 0;

  int curr_cons = values[pos]; // 0 -> Nil, 1 -> Cons
  if (IS_NIL(curr_cons)) {
    return 0;
  }
  // curr_cons is CONS
  size = 1;
  while (pos < max_size - 2 && size >= 0) {
    int next_cons = values[pos + 2];

    if (IS_NIL(next_cons)) {
      pos = ADT_SIZE; // Skip to the end because we have finished
    } else {
      size++;
      pos += 2;
    }
  }
  return size;
}


int main(void) {
  // Allocation (host random states + device random states)
  int * values;
  int values_size = ADT_SIZE * sizeof(int);

  // Host memory
  values = (int*) malloc(values_size);

  /* TIMERS START */
  clock_t t;
  t = clock();

  /* RUN */
  int r;
  long sorted_all = 0;
  int sorted_max_len = -1;
  int max_length = -1;
  int sorted = 0;
  int length = 0;
  int acc_length[MAX_LENGTH] = {0};

  srand(time(NULL));
  for(r = 0; r < R; r++) {
    fill_random_list(values, ADT_SIZE);
    sorted = host_prop_sorted(values, ADT_SIZE);
    length = host_prop_length(values, ADT_SIZE);

    if (sorted >= 0) {
      acc_length[length]++;
    }

    sorted_all += sorted >= 0 ? 1 : 0;
    sorted_max_len = MAX(sorted_max_len, sorted);
    max_length = MAX(max_length, length);
  }

  /* TIMERS END */
  t = clock() - t;
  double time_taken = ((double)t)/(double) CLOCKS_PER_SEC; // calculate the elapsed time
  double list_per_second = (double) sorted_all / time_taken;

  // OUTPUT
  printf("Result: %9ld generated lists (%.02f seconds)\n", R, time_taken);
  printf(" found: %9ld sorted lists (%.04e lists/second)\n", sorted_all, list_per_second);
  printf("        %6d max length (of %d possible)\n", max_length, ADT_SIZE / 2);
  printf("        %6d max length (sorted)\n", sorted_max_len);
  printf("C (secuencial)  & %9d & %.02f & %.04e \n", sorted_all, time_taken, list_per_second);
  printf(" Size distribution:\n");
  print_list(acc_length, MAX_LENGTH);
  printf("   normalized:\n");
  double acc_length_norm[MAX_LENGTH] = {0};
  for (int i = 0; i < MAX_LENGTH; ++i) {
    acc_length_norm[i] = (double) acc_length[i] / sorted_all;
  }
  print_list_double(acc_length_norm, MAX_LENGTH);

  // Clean
  free(values);

  return 0;
}
