{-# LANGUAGE InstanceSigs #-}
module Lib where

import Control.Exception
import Data.Time
import Test.QuickCheck
import System.Environment

import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map

newtype OddList a = OddList { getOddList :: [a] } deriving Show
newtype EvenList a = EvenList { getEvenList :: [a] } deriving Show

instance Arbitrary a => Arbitrary (OddList a) where
  arbitrary :: Gen (OddList a)
  arbitrary = do
    x <- arbitrary
    xs <- getEvenList <$> arbitrary
    return (OddList (x:xs))

instance Arbitrary a => Arbitrary (EvenList a) where
  arbitrary :: Gen (EvenList a)
  arbitrary = oneof
    [
      return (EvenList []),
      do
        x <- arbitrary
        xs <- getOddList <$> arbitrary
        return (EvenList (x:xs))
    ]

isSorted :: Ord a => [a] -> Bool
isSorted (x:y:xs) = x <= y && isSorted (y:xs)
isSorted _ = True

genList :: Gen [Int]
genList = frequency
  [
    (1, return []),
    (4, do
      x <- chooseInt (0, 100)
      xs <- genList
      return (x:xs)
    )
  ]

genList' :: Int -> Gen [Int]
genList' size = do
  raw <- vectorOf (size * 2) (choose (0,100))
  return (parseList raw)
  where
    parseList :: [Int] -> [Int]
    parseList [] = []
    parseList [_] = []
    parseList (x:y:xs) =
      case x `mod` 5 of
        0 -> []
        otherwise -> y:(parseList xs)


countSortedLists :: Int -> Gen (Int, Int, Map Int Double)
-- countSortedLists size = foldr (\l (accN, accMax) -> (accN + 1, max accMax (length l))) (0, 0) . filter isSorted <$> (vector size :: Gen [[Int]])
countSortedLists size = do
  xs <- (vectorOf size (genList :: Gen [Int]))
  let sorted = filter isSorted xs
  let total = length sorted
  let sizesMap = foldr (\xs -> Map.insertWith (+) (length xs) 1) Map.empty sorted
  -- let maxSize = foldr max (-1) (fmap length sorted)
  let maxSize = fst (Map.findMax sizesMap)
  let normalizedSizesMap = Map.map (\a -> a / fromIntegral total) sizesMap
  return (total, maxSize, normalizedSizesMap)

countSortedLists' :: Int -> Int -> Gen (Int, Int, Map Int Double)
countSortedLists' size length' = do
  xs <- (vectorOf size (genList' length' :: Gen [Int]))
  let sorted = filter isSorted xs
  let total = length sorted
  -- let maxSize = foldr max (-1) (fmap length sorted)
  -- return (total, maxSize)
  let sizesMap = foldr (\xs -> Map.insertWith (+) (length xs) 1) Map.empty sorted
  -- let maxSize = foldr max (-1) (fmap length sorted)
  let maxSize = fst (Map.findMax sizesMap)
  let normalizedSizesMap = Map.map (\a -> a / fromIntegral total) sizesMap
  return (total, maxSize, normalizedSizesMap)

main :: IO ()
main = do
    args <- getArgs
    let size = read $ head args
    start <- getCurrentTime
    (result, maxSize, sizesMap) <- generate (countSortedLists size)
    _ <- evaluate result
    putStrLn $ "result: " ++ show result
    putStrLn $ "   max length: " ++ show maxSize
    end <- getCurrentTime
    let diffT = (diffUTCTime end start)
    let listsPerSecond = (round ((fromIntegral result) / diffT)) :: Integer
    putStrLn $ show size ++ " lists in " ++ show diffT ++ "  (" ++ show (listsPerSecond) ++ " lists/s)"
    putStrLn "sizes:"
    print sizesMap

    putStrLn "Variation A: generating lists of fixed size"
    start <- getCurrentTime
    (result', maxSize', sizesMap') <- generate (countSortedLists' size 12)
    _ <- evaluate result
    putStrLn $ "result: " ++ show result'
    putStrLn $ "   max length: " ++ show maxSize'
    end <- getCurrentTime
    let diffT' = (diffUTCTime end start)
    let listsPerSecond' = (round ((fromIntegral result') / diffT')) :: Integer
    putStrLn $ show size ++ " lists in " ++ show diffT' ++ "  (" ++ show (listsPerSecond) ++ " lists/s)"
    putStrLn "sizes:"
    print sizesMap'
