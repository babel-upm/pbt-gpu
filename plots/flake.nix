{
  description = "Latex article template";

  inputs.nixpkgs.url = "nixpkgs";

  outputs = { self, nixpkgs }: {

    devShell.x86_64-linux =
      let
        pkgs = nixpkgs.legacyPackages.x86_64-linux;
      in pkgs.mkShell {
        buildInputs = with pkgs; [
          # Extra (for visualization)
          python310Packages.numpy
          python310Packages.matplotlib

          # Notifications
          libnotify
        ];
      };

  };
}
