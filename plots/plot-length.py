import numpy as np
import matplotlib.pyplot as plt

# C
data = [ 0.4493,0.3594,0.1438,0.0384,0.0077,0.0012,0.0002]

# Haskell
data = [
    0.44787003024486605,
    0.35833358675107274,
    0.1447473534118709,
    3.937027834045971e-2,
    8.109069084153963e-3,
    1.3536090216774953e-3,
    1.912389538790314e-4,
    2.259486000748743e-5,
    2.1497587321296265e-6,
    8.957328050540111e-8]

x = np.arange(0, len(data) + 1)

plt.figure(figsize=(8, 4))

plt.plot(data, marker='o', linestyle='-', color='b', label='C')

# plt.title('Distribution of Sorted Lists by Size')
plt.xlabel('Longitud de la lista')
plt.ylabel('Proporción relativa')
plt.ylim(bottom=0)
plt.xticks(x)  # Ensure x-axis has integer labels
plt.grid(True, which='both', linestyle='--', linewidth=0.5)
plt.tight_layout()

# plt.legend()
plt.savefig("./sorted_lists_distribution.pdf")
plt.savefig("./sorted_lists_distribution.eps")

plt.show()
