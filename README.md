# WIP: Property-based testing using GPU

In this repository you can find an experiment implementing a parallel generation
of algebraic data types using CUDA. There are two main implementations:

- C/CUDA (`examples/c-cuda`): contains a sequential an parallel implementation.
- Haskell/QuickCheck (`examples/haskell-quickcheck`): contains a sequential
  implementation.

## Preliminary results

In a quick example generating and sorting ordered lists, we obtain the following
results:


| Tool               | Implemenation | Sorted | Time    | Sorted/s. |
|--------------------|---------------|--------|---------|-----------|
| Haskell/QuickCheck | Sequential    | 44.6M  | 15.07s. | 2.96M/s.  |
| C                  | Sequential    | 44.7M  | 31.25s. | 1.43M/s.  |
| C/CUDA             | Parallel      | 44.7M  | 2.44s.  | 18.31M/s. |

## Requirements

You should need to have installed the CUDA compiler (`nvcc`) to run the experiments.
